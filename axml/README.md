This is a collection of AXML files that were shipped with SystemInPlace's
ApplianceKit distribution.  They are unmodified, and author contact information,
such as mine, is not up to date.

  -- William Pitcock <nenolod@dereferenced.org>
